/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examlogic_answer;

/**
 *
 * @author accelist
 */
public class Main {

    public void number1() {
        int start1 = 10, start2 = 9;
        int tambah = 1, kurang = 1;

        do {
            System.out.print(start1 + " ");
            System.out.print(start2 + " ");
            start1 = start1 + tambah;
            start2 = start2 - kurang;
        } while (start1 <= 15 && start2 >= 5);
    }

    public void number2() {
        int start1 = 1, start2 = 2;
        int power = 2;
        //1 4 3 64 5 1296 7 32768
        do {
            System.out.println(start1);
            System.out.println((int) Math.pow(start2, power));
            start1 += 2;
            start2 += 2;
            power += 1;
        } while (start1 <= 7);

    }

    public void number3() {
        int numb = 5;
        for (int i = 1; i <= numb; ++i) {
            for (int j = 1; j <= i; ++j) {
                System.out.print(i);
                //i print baris, j print kolom
            }
            System.out.println();
        }
    }

    public void number4() {
        int number = 5;
        while (number > 0) {
            for (int i = 1; i <= number; i++) {
                System.out.print("*");
            }
            System.out.println();
            number--;
        }
    }

    public void number5() {
        int n = 5; // total print *
        int space = n / 2, stNumb = 1;

        // loop buat lines 
        for (int i = 1; i <= n; i++) {
            // loop buat space 
            for (int j = 1; j <= space; j++) {
                System.out.print(" ");
            }

            // loop buat print * 
            for (int x= 1; x <= stNumb; x++) {
                System.out.print("*");
            }

            // buat next line 
            System.out.println();
            if (i <= n / 2) {
                space = space - 1;
                stNumb = stNumb + 2;
            } else {
                space = space + 1;
                stNumb = stNumb - 2;
            }
        }
    }

    public static void main(String args[]) {
        Main answer = new Main();
        //answer.number1();
        answer.number2();
        //answer.number3();
        //answer.number4(); // triangle reverse
        //answer.number5(); // diamond odd number
    }
}
