CREATE DATABASE binusmaya

USE binusmaya

--Relationship: 
-- student can take many courses, 1 course(matakuliah) can taken by many student

--Mata kuliah
CREATE TABLE Course(
	CourseId INT IDENTITY(1,1) PRIMARY KEY,
	CourseName VARCHAR(255) NOT NULL,
	CourseDescription VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

--Mahasiswa
CREATE TABLE Student(
	StudentId INT IDENTITY(1,1) PRIMARY KEY,
	StudentName VARCHAR(255) NOT NULL,
	StudentNim VARCHAR(12) NOT NULL UNIQUE,
	StudentEmail VARCHAR(255) UNIQUE,
	StudentPassword VARCHAR(255) NOT NULL,
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

-- Mata kuliah yg diambil sama mahasiswa
CREATE TABLE StudentCourse(
	StudentCourseId INT IDENTITY(1,1) PRIMARY KEY,
	CourseId INT FOREIGN KEY REFERENCES Course(CourseId),
	StudentId INT FOREIGN KEY REFERENCES Student(StudentId),
	Semester INT, 
)
-- Kelas misalkan LK01
CREATE TABLE Classes( 
	ClassesId INT IDENTITY(1,1) PRIMARY KEY,
	ClassesName VARCHAR(255), --LK01
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE Lecturer(
	LecturerId INT IDENTITY(1,1) PRIMARY KEY,
	LecturerName VARCHAR(255) NOT NULL,
	LecturerEmail VARCHAR(255) UNIQUE,
	LecturerPhone VARCHAR(14),
	StartAt DATETIMEOFFSET,
	ResignAt DATETIMEOFFSET,
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

-- 1 dosen bisa ngajar lebih dri 1 matakuliah, 1 matakuliah bisa di pengang oleh bnyk dosen yg beda
CREATE TABLE CourseLectureDistribution(
	CourseLectureDistributionId INT IDENTITY(1,1) PRIMARY KEY,
	CourseId INT FOREIGN KEY REFERENCES Course(CourseId),
	LecturerId INT FOREIGN KEY REFERENCES Lecturer(LecturerId)
)

CREATE TABLE CourseDetail(
	CourseDetailId INT IDENTITY(1,1) PRIMARY KEY,
	CourseId INT FOREIGN KEY REFERENCES Course(CourseId),
	StudentId INT FOREIGN KEY REFERENCES Student(StudentId),
	LectureId INT FOREIGN KEY REFERENCES Lecturer(LecturerId),
	CourseSession INT,
	Ebook VARCHAR(255),
	PowerPoint VARCHAR(255),
)

--Jadwal nampilin mata kuliah, nama kelas
CREATE TABLE Schedule(
	ScheduleId INT IDENTITY(1,1) PRIMARY KEY,
	ClassesId INT FOREIGN KEY REFERENCES Classes(ClassesId),
	CourseId INT FOREIGN KEY REFERENCES Course(CourseId),
	TimeSchedule DATETIMEOFFSET,
	Room VARCHAR(255),
	DescriptionSchedule VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE Major(
	MajorId INT IDENTITY(1,1) PRIMARY KEY,
	StudentId INT FOREIGN KEY REFERENCES Student(StudentId), -- satu major bisa punya bnyk student
	MajorName VARCHAR(255),
)

CREATE TABLE Score(
	ScoreId INT IDENTITY(1,1) PRIMARY KEY,
	StudentCourse INT FOREIGN KEY REFERENCES StudentCourse(StudentCourseId),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE ForumHeader(
	ForumHeaderId INT IDENTITY(1,1) PRIMARY KEY,
	ForumHeaderName VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE ForumDetail(
	ForumDetailId INT IDENTITY(1,1) PRIMARY KEY,
	ForumHeaderId INT FOREIGN KEY REFERENCES ForumHeader(ForumHeaderId),
	StudentCourseId INT FOREIGN KEY REFERENCES StudentCourse(StudentCourseId),
	LecturerId INT FOREIGN KEY REFERENCES Lecturer(LecturerId),
	ForumTitle VARCHAR(255) NOT NULL,
	ForumDescription VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE FinancialHeader(
	FinancialHeaderId INT IDENTITY(1,1) PRIMARY KEY,
	TransactionHeaderName VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE FinancialDetail(
	FinancialDetailId INT IDENTITY(1,1) PRIMARY KEY,
	FinancialHeaderId INT FOREIGN KEY REFERENCES FinancialHeader(FinancialHeaderId),
	Studentid INT FOREIGN KEY REFERENCES Student(StudentId),
	Charge INT,
	Payment INT,
	TransactionDate DATETIMEOFFSET,
	TransactionStatus VARCHAR(255),
	TransactionDescription VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE AttendanceHeader(
	AttendanceHeaderId INT IDENTITY(1,1) PRIMARY KEY,
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE AttendanceDetail(
	AttendanceDetailId INT IDENTITY(1,1) PRIMARY KEY,
	AttendanceHeaderId INT FOREIGN KEY REFERENCES AttendanceHeader(AttendanceHeaderId),
	StudentId INT FOREIGN KEY REFERENCES Student(StudentId),
	ScheduleId INT FOREIGN KEY REFERENCES Schedule(ScheduleId),
	AttendanceSession INT,
	Note VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE MessageHeader(
	MessageHeaderId INT IDENTITY(1,1) PRIMARY KEY,
	MessageSubject VARCHAR(255),
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)

CREATE TABLE MessageDetail(
	MessageDetailId INT IDENTITY(1,1) PRIMARY KEY,
	MessageHeaderId INT FOREIGN KEY REFERENCES MessageHeader(MessageHeaderId),
	StudentId INT FOREIGN KEY REFERENCES Student(StudentId),
	MessageTitle VARCHAR(255) NOT NULL,
	MessageDescription VARCHAR(355) ,
	MessageDate DATETIMEOFFSET,
	CreatedAt DATETIMEOFFSET DEFAULT getdate(),
	CreatedBy VARCHAR(255) DEFAULT 'SYSTEM',	
	UpdatedAt DATETIMEOFFSET DEFAULT getdate(),
	UpdatedBy VARCHAR(255) DEFAULT 'SYSTEM'
)