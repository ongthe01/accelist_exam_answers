package com.latihan.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.latihan" })
@EntityScan("com.latihan.model")
@EnableJpaRepositories("com.latihan.dao")
public class LatihanCaseHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(LatihanCaseHibernateApplication.class, args);
	}

}