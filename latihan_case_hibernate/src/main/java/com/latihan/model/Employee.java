package com.latihan.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="employee")
@DynamicInsert

public class Employee implements Serializable{
	
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "employee_outlets", joinColumns = {
			@JoinColumn(name = "employee_id", referencedColumnName = "id", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "outlet_id", referencedColumnName = "id", nullable = false) })
	
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<Outlet> outlet;
	
	
	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "phone")
	private String phoneNumber;

	@Column(name = "emergencycontact", nullable = true)
	private String emergencyContact;

	@Column(name = "wage")
	private int wage;

	@Column(name = "employedat")
	private Date employedAt;

	@Column(name = "resignation", columnDefinition = "varchar(255) default 'NOT RESIGNED'")
	private String resign;

	@Column(name = "createdat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdAt;

	@Column(name = "createdby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String createdBy;

	@Column(name = "updatedat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedAt;

	@Column(name = "updatedby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public int getWage() {
		return wage;
	}

	public void setWage(int wage) {
		this.wage = wage;
	}

	public Date getEmployedAt() {
		return employedAt;
	}

	public void setEmployedAt(Date employedAt) {
		this.employedAt = employedAt;
	}

	public String getResign() {
		return resign;
	}

	public void setResign(String resign) {
		this.resign = resign;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Set<Outlet> getOutlet() {
		return outlet;
	}

	public void setOutlet(Set<Outlet> outlet) {
		this.outlet = outlet;
	}

	public void setOutlet(Outlet outlet) {
		getOutlet().add(outlet);
		outlet.setEmployee(this);
	}
	
}