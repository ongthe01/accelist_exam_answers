package com.latihan.dao;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.latihan.model.Outlet;

@Transactional
@Repository
public interface OutletDao extends JpaRepository<Outlet, Long>{
	List<Outlet> findByEmployeeId(Long employeeId);
}