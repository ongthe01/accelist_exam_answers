package com.latihan.dao;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.latihan.model.*;

@Transactional
@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long>{
	List<Employee> findByOutletId(Long outletId);
}