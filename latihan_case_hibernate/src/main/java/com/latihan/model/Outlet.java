package com.latihan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "outlet")
@DynamicInsert
public class Outlet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "outlet", fetch = FetchType.LAZY )
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<Employee> employee;
	
	@Column(name = "name")
	private String outletName;
	
	@Column(name = "address")
	private String address;

	@Column(name = "createdat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdAt;

	@Column(name = "createdby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String createdBy;

	@Column(name = "updatedat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedAt;

	@Column(name = "updatedby", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String updatedBy;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOutletName() {
		return outletName;
	}

	public void setOutletName(String outletName) {
		this.outletName = outletName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Set<Employee> employee) {
		this.employee = employee;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public void setEmployee(Employee employee) {
		getEmployee().add(employee);
		employee.setOutlet(this);
	}
	
}