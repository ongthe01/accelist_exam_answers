package com.latihan.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.latihan.dao.*;
import com.latihan.exception.ResourceNotFoundException;
import com.latihan.model.*;

@RestController
public class OutletController {
	@Autowired
	private OutletDao or;
	
	@Autowired
	private EmployeeDao er;
	
	@GetMapping("/outlet")
	public Page<Outlet> getOutlet(Pageable pageable) {
		return or.findAll(pageable);
	}
	
	@PostMapping("/outlet")
	public Outlet createOutlet(@Valid @RequestBody Outlet outlet) {
		return or.save(outlet);
	}
	
	@PutMapping("/outlet/{outletId}")
	public Outlet updateOutlet(@PathVariable Long outletId, @Valid @RequestBody Outlet outletRequest) {
		return or.findById(outletId).map(outlet->{
			outlet.setOutletName(outletRequest.getOutletName());
			outlet.setAddress(outletRequest.getAddress());
			return or.save(outlet);
		}).orElseThrow(()->new ResourceNotFoundException("Outlet Id not found with id " + outletId));
				
	}
	
	@DeleteMapping("outlet/{outletId}")
	public ResponseEntity<?> deleteOutlet(@PathVariable Long outletId){
		return or.findById(outletId).map(outlet->{
			or.delete(outlet);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new ResourceNotFoundException("Outlet Id not found with id " + outletId));
				
	}
	
}