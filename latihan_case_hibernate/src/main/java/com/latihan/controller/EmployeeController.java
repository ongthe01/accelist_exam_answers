package com.latihan.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.latihan.exception.*;
import com.latihan.model.*;
import com.latihan.dao.*;

@RestController
public class EmployeeController {
    
	@Autowired
	private EmployeeDao er;
	
	@Autowired
	private OutletDao or;
	
	@GetMapping("/employee")
	public Page<Employee> getEmployee(Pageable pageable) {
		return er.findAll(pageable);
		
	}
	
	//add employee
	@PostMapping("/employee/{outletId}")
	public Employee createEmployee(@PathVariable Long outletId, @Valid @RequestBody Employee employee) {
		return or.findById(outletId).map(outlet->{
			
			employee.setName(employee.getName());
			employee.setAddress(employee.getAddress());
			employee.setPhoneNumber(employee.getPhoneNumber());
			employee.setEmergencyContact(employee.getEmergencyContact());
			employee.setWage(employee.getWage());
			employee.setEmployedAt(employee.getEmployedAt());
			employee.getOutlet().add(outlet);
			outlet.getEmployee().add(employee);
			return er.save(employee);
		}).orElseThrow(()-> new ResourceNotFoundException("Outlet id not found with id " + outletId));
		
		
	}
	
	//update employee
	@PutMapping("/employee/{employeeId}")
	public Employee updateEmployee(@PathVariable Long employeeId, @Valid @RequestBody Employee employeeRequest) {
		return er.findById(employeeId).map(employee->{
			employee.setName(employeeRequest.getName());
			employee.setAddress(employeeRequest.getAddress());
			employee.setPhoneNumber(employeeRequest.getPhoneNumber());
			employee.setEmergencyContact(employeeRequest.getEmergencyContact());
			return er.save(employee);
		}).orElseThrow(()->new ResourceNotFoundException("Employee Id not found with id " + employeeId));
				
	}
	
	//delete employee
	@DeleteMapping("employee/{employeeId}")
	public ResponseEntity<?> deleteEmployee(@PathVariable Long employeeId){
		return er.findById(employeeId).map(employee->{
			er.delete(employee);
			return ResponseEntity.ok().build();
		}).orElseThrow(()-> new ResourceNotFoundException("Employee Id not found with id " + employeeId));
				
	}
	
	//get outlet
	@GetMapping("outlet/{outletId}/employee")
	public List<Employee> getEmployeeByOutletId(@PathVariable Long outletId){
		return er.findByOutletId(outletId);
	}
	
}